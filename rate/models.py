from django.db import models


class Rate(models.Model):
    relation_main_currency = models.DecimalField(max_digits=10, decimal_places=4)
    date = models.DateField(format('%m/%d/%y'))
    currencies_to = models.CharField(max_length=4)
    currencies_from = models.CharField(max_length=4)

    # amount = models.DecimalField(max_digits=10, decimal_places=2)

    @classmethod
    def get_available_choices(cls):
        choices = ()
        for choice in cls.objects.all():
            choices = choices + ((choice.currencies_to, choice.currencies_to),)

        return choices


