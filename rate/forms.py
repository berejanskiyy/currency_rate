from django import forms
from .models import Rate


class RateForm(forms.ModelForm):
    amount = forms.DecimalField(max_digits=10, decimal_places=2)

    class Meta:

        model = Rate
        # currencies_to = forms.ChoiceField(choices=Rate.get_available_choices(), label="",
        currencies_to = forms.ChoiceField(choices=Rate.objects.values('currencies_to'), label="currencies_to",
                                          initial='', widget=forms.Select(),
                                          required=True)
        fields = ('currencies_to', 'amount', 'currencies_from')
