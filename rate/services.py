from .models import Rate


def currency_conversion(amount, currencies_from, currencies_to, currencies_main="EUR"):

    rate_relation_main_currency = Rate.objects.filter(currencies_from=currencies_from, currencies_to=currencies_main)
    rate_relation_our_currency = Rate.objects.filter(currencies_from=currencies_main, currencies_to=currencies_to)

    amount_main_currency = amount / rate_relation_main_currency.relation_main_currency
    amount_our_currency = amount_main_currency * rate_relation_our_currency.relation_main_currency

    return amount_our_currency

