from django.urls import path

from . import views

urlpatterns = [
    path('', views.rform),
    path('rate/result/',views.result)
]