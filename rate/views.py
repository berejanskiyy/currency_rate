from django.shortcuts import render
from rate.forms import RateForm
from .models import Rate
from django.shortcuts import redirect
from django.utils import timezone
from . import services


def rform(request):
    rates = Rate.objects.filter(date=timezone.now().today())
    if request.method == "POST":
        form = RateForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.date = timezone.now()
            post.relation_main_currency = 0.1
            post.save()
            return redirect('rate/result', pk=post.pk)
    else:
        form = RateForm()

    return render(request, 'rate/rform.html', {'rates': rates, 'form': form})


def result(request):
    rates = Rate.objects.filter(date=timezone.now().today())
    count = Rate.objects.filter(date=timezone.now().today()).count()
    return render(request, 'rate/result.html', {'rates': rates, 'count': count})


def currency_change(request):
    form = RateForm()

    return render(request, 'rate/rform.html', {'form': form})
