import os, sys

proj_path = "/home/s/PycharmProjects/currency_rate"
# This is so Django knows where to find stuff.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "currency_rate.settings")
sys.path.append(proj_path)

# This is so my local_settings.py gets loaded.
os.chdir(proj_path)

# This is so models get loaded.
from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()

import requests

from django.db import models

from rate.models import Rate
from rate import services
from django.conf import settings

response = requests.get(settings.REQUEST_URL + '?' + settings.ACCESS_KEY)

rates = response.json()['rates']



def change_base_currency(rates, new_base):
    relation_main = rates[new_base]
    new_rates = {}
    for rate in rates:
        relation = rates[rate]
        new_relation = relation / relation_main
        new_rates[rate] = new_relation
    return new_rates


new_base = 'UAH'
new_rates = change_base_currency(rates, new_base)

rates = response.json()['rates']
date = response.json()['date']

for rate in new_rates:
    new_relation = new_rates[rate]
    Rate.objects.create(currencies_to=rate, currencies_from=new_base, date=date, relation_main_currency=new_relation)
